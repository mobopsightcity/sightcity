package com.example.sightcity;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;

public class CustomEditText extends EditText {
	
	public CustomEditText(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	public CustomEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}
	 @Override
	    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
	        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK
	                && event.getAction() == KeyEvent.ACTION_UP) {
	        	View view = SearchActivity.instance.getCurrentFocus();
	        	if (view != null) {  
	        	    InputMethodManager imm = (InputMethodManager)SearchActivity.instance.getSystemService(Context.INPUT_METHOD_SERVICE);
	        	    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		        	if(SearchActivity.instance.keyboardVisible){
		        		SearchActivity.instance.keyboardVisible= false;
						RelativeLayout layout = (RelativeLayout)SearchActivity.instance.findViewById(R.id.search_for_holder);
						if(layout.getLayoutParams() instanceof ViewGroup.MarginLayoutParams){
							ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) layout.getLayoutParams();
					        p.setMargins(0, 0, 0, 0);
					        layout.requestLayout();
						}
					}
	        	}
	            return true;
	        }
	        return super.dispatchKeyEvent(event);
	  }

	

}
