package com.example.sightcity;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

public class CustomCompassListener implements SensorEventListener {
	Activity activity;

	public CustomCompassListener(Activity act) {
		this.activity = act;
	}
	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChanged(SensorEvent arg0) {
		// TODO Auto-generated method stub
		MainActivity.lastCompass = arg0.values;
	}
}