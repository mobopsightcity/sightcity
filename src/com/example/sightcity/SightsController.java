package com.example.sightcity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.example.sightcity.Sight.sightType;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.hardware.SensorManager;
import android.location.Location;
import android.util.Xml.Encoding;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;

public class SightsController {
	private ArrayList<Sight> sights;
	private Context context;
	public static SightsController instance;
	public SightsController(Context context){
		this.context = context;
		instance = this;
	}
	public ArrayList<Sight> getSights(){
		return sights;
	}
	public void fetchSights(){
		sights = new ArrayList<Sight>();
		List<HashMap<String, String>> list = MainActivity.instance.placesC.places;
		for(int i=0;i<list.size();i++){
			Sight tmp = new Sight();
			Location tmp_loc = new Location("manual");
			tmp_loc.setLatitude(Double.parseDouble(list.get(i).get("lat")));
			tmp_loc.setLongitude(Double.parseDouble(list.get(i).get("lng")));
			tmp_loc.setAltitude(Double.parseDouble(list.get(i).get("alt")));
			tmp.coord = tmp_loc;
			tmp.lowpass = new AngleLowpassFilter[3];
			tmp.type = sightType.caffe;
			tmp.name = list.get(i).get("place_name");
			tmp.vicinity = list.get(i).get("vicinity");
			tmp.iconUrl = list.get(i).get("icon");
			tmp.imageReference = list.get(i).get("photos");
			tmp.bitmapId = R.drawable.coffee;
			String types = list.get(i).get("types");
			if(types.contains("cafe")){
				tmp.bitmapId = R.drawable.coffee;
			}
			else{
				if(types.contains("museum")){
					tmp.bitmapId = R.drawable.white_library;
				}
				else{
					if(types.contains("church")){
						
					}
					else{
						if(types.contains("park")){
							if(types.contains("campground")){
								tmp.bitmapId = R.drawable.camp;
							}
							else{
								tmp.bitmapId = R.drawable.park;
							}
						}
						else{
							if(types.contains("campground")){
								tmp.bitmapId = R.drawable.camp;
							}
						}
					}
				}
			}
			for (int j = 0; j < 3; j++) {
				int length;
				if (j == 2)
					length = 40;
				else {
					if (j == 1) {
						length = 40;
					} else {
						length = 40;
					}
				}
				tmp.lowpass[j] = new AngleLowpassFilter(length);
			}
			sights.add(tmp);
		}
		
	}
	public void updateSightsCanvas(){
		if(sights!=null){
		for (Sight l : sights) {
			try {
				if (MainActivity.gotLocation && MainActivity.gotAcc) {
					float curBearingTothis = MainActivity.lastCoordinate.bearingTo(l.coord);
					float rotation[] = new float[9];
					float identity[] = new float[9];
					boolean gotRotation = SensorManager.getRotationMatrix(rotation, identity, MainActivity.lastAccelerometer,MainActivity.lastCompass);
					float orientation[] = new float[3];
					if (gotRotation) {
						// orientation vector
						float cameraRotation[] = new float[9];
						// remap such that the camera is pointing straight down
						// the
						// Y axis
						SensorManager.remapCoordinateSystem(rotation,SensorManager.AXIS_X, SensorManager.AXIS_Z,
								cameraRotation);

						// orientation vector
						SensorManager.getOrientation(cameraRotation,
								orientation);
					}
					
					l.LastOrientation = new float[3];
					//getting smooth values
					for(int i=0;i<3;i++){
						l.lowpass[i].add(orientation[i]);
						l.LastOrientation[i] = l.lowpass[i].average();
					}
					l.curBearingToThisSight = curBearingTothis;
					
				}
			} catch (Exception e) {
				System.out.println("error:"+e);
			}
		}
		}
	}
}
