package com.example.sightcity;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

public class CustomSensorEventListener implements SensorEventListener {
	Activity activity;

	public CustomSensorEventListener(Activity act) {
		this.activity = act;
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSensorChanged(SensorEvent arg0) {
		// TODO Auto-generated method stub
		MainActivity.lastAccelerometer = arg0.values;
		MainActivity.gotAcc = true;
	}

}