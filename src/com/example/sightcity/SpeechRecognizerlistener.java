package com.example.sightcity;

import java.io.File;
import java.util.ArrayList;

import com.example.sightcity.services.CommonConstants;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.SpeechRecognizer;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class SpeechRecognizerlistener implements RecognitionListener {
	private Context context;
	private MediaPlayer mp = new MediaPlayer();
	private boolean found = false;

	public SpeechRecognizerlistener(Context context) {
		super();
		this.context = context;
	}

	@Override
	public void onBeginningOfSpeech() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onBufferReceived(byte[] arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEndOfSpeech() {
		// TODO Auto-generated method stub

		Log.d("TESTING: End of speach", "ready");
	}

	@Override
	public void onError(int arg0) {
		// TODO Auto-generated method stub

		Log.d("TESTING: error", "ready");
		if (arg0 == 6) {
			SearchActivity.mIsListening = false;
			SearchActivity.canChangeGif = false;
			SearchActivity.instance.startListening();
		} else {
			SearchView.instance.changeGif(CommonConstants.GIF_Normal);
			Toast.makeText(context, "error " + arg0, Toast.LENGTH_SHORT).show();
			SearchActivity.mIsListening = false;
			SearchActivity.canChangeGif = true;
		}
	}

	@Override
	public void onEvent(int arg0, Bundle arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPartialResults(Bundle arg0) {
		// TODO Auto-generated method stub

		Log.d("TESTING: Ready for partial results", "ready");
	}

	@Override
	public void onReadyForSpeech(Bundle arg0) {
		// TODO Auto-generated method stub
		SearchView.instance.changeGif(CommonConstants.GIF_LISTENING);
	}

	@Override
	public void onResults(Bundle results) {
		// TODO Auto-generated method stub
		// String str = new String();
		// Log.d(TAG, "onResults " + results);
		ArrayList data = results
				.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);

		// if(data.size() >=1){
		// //check for save it:
		// }
		String search_for = "";

		String[] array = SearchActivity.instance.getResources().getStringArray(
				R.array.place_type_name);
		ArrayList<String> arraylist = new ArrayList<String>();
		for (String string : array) {
			arraylist.add(string.toLowerCase());
		}
		for (int i = 0; i < data.size(); i++) {
			String temp = ((String)data.get(i)).toLowerCase();
			if (temp.startsWith("search for")) {
				if (arraylist.contains(temp.replace("search for ", ""))) {
					found = true;
					search_for = temp.replace("search for ", "");
					break;
				}
			}
		}
		if (found) {
			found = false;
			Intent activityIntent;
			if (MainActivity.instance == null) {
				activityIntent = new Intent(this.context, MainActivity.class);
			} else {
				activityIntent = MainActivity.instance.getIntent();
			}
			SearchActivity.mIsListening = false;
			activityIntent.putExtra("open_tab", true);
			activityIntent.putExtra("search_for_type", search_for);
			activityIntent.putExtra("search_for_specific", "");
			activityIntent = activityIntent
					.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			SearchView.instance.changeGif(CommonConstants.GIF_SEARCHING);
			SearchActivity.mIsListening = false;
			SearchActivity.instance.startActivity(activityIntent);
			SearchActivity.instance.overridePendingTransition(R.anim.slide_in,
					R.anim.slide_out);
		} else {
			SearchView.instance.changeGif(CommonConstants.GIF_Normal);
			SearchActivity.mIsListening = false;
			playAudio(R.raw.sorry_i_got_nothing);
			try{
				SearchActivity.instance.keyboardVisible = false;
				View view = SearchActivity.instance.getCurrentFocus();
				InputMethodManager imm = (InputMethodManager)
		                SearchActivity.instance.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
				}
				catch(Exception e){
					
				}
				RelativeLayout layout = (RelativeLayout)SearchActivity.instance.findViewById(R.id.search_for_holder);
				if(layout.getLayoutParams() instanceof ViewGroup.MarginLayoutParams){
					ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) layout.getLayoutParams();
			        p.setMargins(0, 0, 0, 0);
			        layout.requestLayout();
				}
		}
		// Log.d("TESTING: SPEECH SERVICE: CALL START", "onResults()");
	}

	@Override
	public void onRmsChanged(float arg0) {
		// TODO Auto-generated method stub
	}
	public void stopAudio(){
		mp.stop();
	}
	public void playAudio(int fileid){
	    //set up MediaPlayer    

	    try {
	    	mp = MediaPlayer.create(this.context,fileid);
	    	mp.start();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}

}
