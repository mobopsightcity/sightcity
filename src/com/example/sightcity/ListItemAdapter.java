package com.example.sightcity;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ListItemAdapter extends ArrayAdapter<ListItem> {
    public ListItemAdapter(Context context, ArrayList<ListItem> users) {
       super(context, 0, users);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       // Get the data item for this position
    	ListItem item = getItem(position);    
       // Check if an existing view is being reused, otherwise inflate the view
       if (convertView == null) {
          convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_list_item, parent, false);
       }
       // Lookup view for data population
       TextView commandName = (TextView) convertView.findViewById(R.id.commandName);
       // Populate the data into the template view using the data object
       commandName.setText(item.description);
       // Return the completed view to render on screen
       return convertView;
   }
}