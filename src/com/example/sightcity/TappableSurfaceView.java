package com.example.sightcity;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.example.sightcity.MainActivity.PlaceholderFragment;

import android.R.color;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Movie;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.opengl.Visibility;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TappableSurfaceView extends SurfaceView implements OnTouchListener {
	private SightsController controller;
	private long loaderStart;
	private Movie loaderGif;
	private boolean drawed_info;
	private boolean canshowDesc;
	public Bitmap imageSight;
	private String description;
	private String Title;
	public boolean hideLoader;
	public static TappableSurfaceView instance;

	private void init(Context context) {
		controller = new SightsController(context);
		this.setWillNotDraw(false);
		loaderStart = 0;
		InputStream is = this.getResources().openRawResource(R.drawable.loader);
		loaderGif = Movie.decodeStream(is);
		this.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		drawed_info = false;
		canshowDesc = false;
		instance = this;
		hideLoader = false;
	}

	public TappableSurfaceView(Context context) {
		super(context);
		init(context);
	}

	public TappableSurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	public TappableSurfaceView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public void updateInfo(String url, String Title, String description)
			throws IOException {
		hideLoader = false;
		this.Title = Title;
		this.description = description;
		canshowDesc = true;
		BitmapWorkerTask task = new BitmapWorkerTask(this);
		task.execute(url);
	}
	private ArrayList<Sight> checkClickedSights(float x,float y){
		ArrayList<Sight> sights = controller.getSights();
		ArrayList<Sight> list = new ArrayList<Sight>();
		for(Sight l :sights){
			if(x>l.xOnScreen-50 && x<l.xOnScreen+50 && y>l.yOnScreen-50 && y<l.yOnScreen+50){
				list.add(l);
			}
		}
		return list;
	}




	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		controller.updateSightsCanvas();
		ArrayList<Sight> sights = controller.getSights();
		if (MainActivity.canStartDrawingPlaces) {
			try {

				canvas.drawColor(Color.TRANSPARENT, Mode.CLEAR);
				for (Sight l : sights) {
					canvas.save();
					Paint targetPaint = new Paint();
					/*
					 * canvas.rotate((float) (360 - Math
					 * .toDegrees(l.LastOrientation[2])));
					 */
					// Translate, but normalize for the FOV of the camera --
					// basically, pixels per degree, times degrees == pixels
					float degress = (float) Math
							.toDegrees(l.LastOrientation[0]);

					float bearing = l.curBearingToThisSight;
					float angle = (degress) - bearing;
					float dx = (float) ((canvas.getWidth() / PlaceholderFragment.horizontalFOV) * (angle));
					float dy = (float) ((canvas.getHeight() / PlaceholderFragment.verticalFOV) * Math
							.toDegrees(l.LastOrientation[1]));
					double deviation = MainActivity.lastCoordinate
							.getAltitude() - l.coord.getAltitude();
					dy = (float) (dy);
					// wait to translate the dx so the horizon doesn't get
					// pushed off
					canvas.translate(0.0f, (float) (0.0f - dy + deviation));

					l.yOnScreen= canvas.getHeight() / 2 +(float) (-dy+deviation);
					targetPaint.setColor(Color.RED);
					// make our line big enough to draw regardless of
					// rotation and translation

					/*
					 * canvas.drawLine(0f - canvas.getHeight(),
					 * canvas.getHeight() / 2, canvas.getWidth() +
					 * canvas.getHeight(), canvas.getHeight() / 2, targetPaint);
					 */

					// now translate the dx
					// l.view.translate(-dx, 0);
					canvas.translate(0.0f - dx, 0.0f);
					l.xOnScreen=canvas.getWidth() / 2-dx;
					// draw our point -- we've rotated and translated this
					// to the right spot already

					Bitmap b = BitmapFactory.decodeResource(getResources(),
							l.bitmapId);
					targetPaint.setColor(Color.parseColor("#ff4ba587"));

					canvas.drawRect(canvas.getWidth() / 2 - 50,
							canvas.getHeight() / 2 - 50,
							canvas.getWidth() / 2 + 50,
							canvas.getHeight() / 2 + 50, targetPaint);
					targetPaint.setColor(Color.WHITE);
					canvas.drawBitmap(b, canvas.getWidth() / 2 - 35,
							canvas.getHeight() / 2 - 35, targetPaint);
					canvas.restore();

					// drawing information
					if (canshowDesc) {
						if (!drawed_info) {
							targetPaint.setColor(Color.parseColor("#ff4ba587"));
							canvas.drawRect(50,
									3 * canvas.getHeight() / 4 - 40,
									3 * canvas.getWidth() / 4,
									3 * canvas.getHeight() / 4 + 40,
									targetPaint);
							targetPaint.setColor(Color.WHITE);
							canvas.drawRect(50,
									3 * canvas.getHeight() / 4 + 40,
									3 * canvas.getWidth() / 4,
									3 * canvas.getHeight() / 4 + 170,
									targetPaint);
							if (!hideLoader) {
								int duration = loaderGif.duration();
								long now = android.os.SystemClock
										.uptimeMillis();
								if (loaderStart == 0) { // first time
									loaderStart = now;
								}
								int relTime = (int) ((now - loaderStart) % duration);
								loaderGif.setTime(relTime);
								loaderGif.draw(canvas, 75,
										3 * canvas.getHeight() / 4 + 55);
							} else {
								if (imageSight != null) {
									canvas.drawBitmap(imageSight, 65,
											3 * canvas.getHeight() / 4 + 55,
											targetPaint);
								}
							}
							targetPaint.setColor(Color.BLACK);
							canvas.save();
							canvas.restore();
							// canvas.drawText(text, x, y, paint);
							drawed_info = true;
						}
					}
				}

			} catch (Exception e) {
				System.out.println(e);
				// Log.println(1, "", message);
			}
		}
		invalidate();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		try{
		float positionX = event.getX();
		float positionY = event.getY();
		ArrayList<Sight> sights = checkClickedSights(positionX,positionY);
		if(sights.size()>0){
			Sight sightShow = sights.get(0);
			LinearLayout linear = (LinearLayout)PlaceholderFragment.rootView.findViewById(R.id.informationHolder);
			linear.setVisibility(VISIBLE);
			String image = sightShow.imageReference;
			MainActivity.instance.placesC.getPlaceImage(image);
			TextView textview = (TextView)PlaceholderFragment.rootView.findViewById(R.id.SightName);
			textview.setText(sightShow.name);
			textview = (TextView)PlaceholderFragment.rootView.findViewById(R.id.SightVicinity);
			textview.setText(sightShow.vicinity);
			ImageButton button1 = (ImageButton)PlaceholderFragment.rootView.findViewById(R.id.pathButton);
			ImageButton button2 = (ImageButton)PlaceholderFragment.rootView.findViewById(R.id.ImageButton);
			ImageButton button3 = (ImageButton)PlaceholderFragment.rootView.findViewById(R.id.FavouriteButton);
			float buttonwidth = PlaceholderFragment.rootView.getWidth()/3;
			float leftovers = PlaceholderFragment.rootView.getWidth() - 3*((int)buttonwidth);
			if(button1.getLayoutParams() instanceof ViewGroup.MarginLayoutParams){
				ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) button1.getLayoutParams();
		        p.width=(int) buttonwidth;
		        p.setMargins((int) (leftovers/2), 0, 0, 0);
		        button1.requestLayout();
			}
			if(button2.getLayoutParams() instanceof ViewGroup.MarginLayoutParams){
				ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) button2.getLayoutParams();
		        p.width=(int) buttonwidth;
		        button2.requestLayout();
			}
			if(button3.getLayoutParams() instanceof ViewGroup.MarginLayoutParams){
				ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) button3.getLayoutParams();
		        p.width=(int) buttonwidth;
		        button3.requestLayout();
			}
			}
		}
		catch(Exception e){
			
		}
		//this.getParent().;
		
		return false;
	}
	@Override
	public boolean performClick() {
		// TODO Auto-generated method stub
		return super.performClick();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		performClick();
		return false;
	}
}
