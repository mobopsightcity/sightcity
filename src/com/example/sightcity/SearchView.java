package com.example.sightcity;

import java.io.InputStream;
import java.util.ArrayList;

import com.example.sightcity.services.CommonConstants;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.os.Bundle;
import android.renderscript.Sampler;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class SearchView extends SurfaceView {
	
	private Movie mMovie;
	private long mMovieStart;
	private boolean canDraw=false;
	public static SearchView instance;
	public SearchView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		init();
	}

	public SearchView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		init();
	}
	
	public SearchView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		init();
	}
	private void init(){
		instance = this;
		InputStream is = this.getResources().openRawResource(R.drawable.jarvis_normal);
		mMovie = Movie.decodeStream(is);
		canDraw = true;
		this.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		
	}


	public void changeGif(int gif){

		TextView bigText = (TextView)SearchActivity.instance.findViewById(R.id.big_text);
		TextView smallText = (TextView)SearchActivity.instance.findViewById(R.id.small_text);
		ImageButton button = (ImageButton)SearchActivity.instance.findViewById(R.id.btnSpeak);
		EditText searchText = (EditText)SearchActivity.instance.findViewById(R.id.search_for);
		ListView view = (ListView)SearchActivity.instance.findViewById(R.id.commands);
		if (gif == CommonConstants.GIF_GETTING_READY) {
			bigText.setText(R.string.getting_ready_text);
			smallText.setText("");
			button.setVisibility(INVISIBLE);
			searchText.setVisibility(INVISIBLE);
			view.setVisibility(INVISIBLE);
		} else if (gif == CommonConstants.GIF_LISTENING) {
			bigText.setText(R.string.listening_text);
			smallText.setText("");
			button.setVisibility(INVISIBLE);
			searchText.setVisibility(INVISIBLE);
			view.setVisibility(INVISIBLE);
		} else if (gif == CommonConstants.GIF_SEARCHING) {
			bigText.setText(R.string.searching_text);
			smallText.setText("");
			button.setVisibility(INVISIBLE);
			searchText.setVisibility(INVISIBLE);
			view.setVisibility(INVISIBLE);
		} else if (gif == CommonConstants.GIF_Normal) {
			bigText.setText(R.string.hello_text);
			smallText.setText(R.string.try_text);
			button.setVisibility(VISIBLE);
			searchText.setVisibility(VISIBLE);
		} else throw new IllegalArgumentException("invalid value of argument gif: " + gif);
		canDraw = false;
		InputStream is = this.getResources().openRawResource(gif);
		mMovie = Movie.decodeStream(is);
		canDraw = true;
	}
	@Override
	protected void onDraw(Canvas canvas) {
		if(canDraw){
		 long now = android.os.SystemClock.uptimeMillis();
	      if (mMovieStart == 0) { // first time
	        mMovieStart = now;
	      }
	      if (mMovie != null) {
	        int dur = mMovie.duration();
	        if (dur == 0) {
	          dur = 1000;
	        }
	        int relTime = (int) ((now - mMovieStart) % dur);
	        mMovie.setTime(relTime);
	        try{
	        	int w = mMovie.width();
	        	int h = mMovie.height();
	        mMovie.draw(canvas, getWidth()/2 - mMovie.width()/2, mMovie.height()/3+30);
	        }
	        catch(Exception e){
	        	String m = e.getMessage();
	        	Log.println(1, "", m);
	        }
	        invalidate();
	      }
		}
	}
	
}
