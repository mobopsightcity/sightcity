package com.example.sightcity;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

public class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
    private final WeakReference<TappableSurfaceView> cameraViewReference;
    private String data = "";

    public BitmapWorkerTask(TappableSurfaceView view) {
        // Use a WeakReference to ensure the ImageView can be garbage collected
    	cameraViewReference = new WeakReference<TappableSurfaceView>(view);
    }

    // Decode image in background.
    @Override
    protected Bitmap doInBackground(String... params) {
        data = params[0];
        InputStream is;
        Bitmap imageSight = null;
	
			try {
				URL url = new URL(data);
				is = url.openStream();
				imageSight = BitmapFactory.decodeStream(is);
		        imageSight = Bitmap.createScaledBitmap(imageSight, 100, 100, false);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		
        
        return imageSight;
    }

    // Once complete, see if ImageView is still around and set bitmap.
    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (cameraViewReference != null && bitmap != null) {
            cameraViewReference.get().imageSight = bitmap;
            cameraViewReference.get().hideLoader = true;

        }
        //SightView.clicked = false;
    }
}