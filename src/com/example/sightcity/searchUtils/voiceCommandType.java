package com.example.sightcity.searchUtils;

public class voiceCommandType {
	public enum commandType {
		SEARCH_FOR_TYPE,
		SEARCH_FOR_SPECIFIC,
		SEARCH_FOR_PREFERENCES
	}
	public commandType type;
	public String searchForText;
	public voiceCommandType(commandType type,String searchText){
		this.type = type;
		this.searchForText = searchText;
	}
	
}
