package com.example.sightcity;

import java.util.ArrayDeque;

public class AngleLowpassFilter {

    private  int LENGTH;

    private float sumSin, sumCos, smooth=1f;
    private float first;
    private ArrayDeque<Float> queue = new ArrayDeque<Float>();
    public AngleLowpassFilter(int length){
    	LENGTH = length;
    }

    public void add(float radians){

        sumSin += (float) Math.sin(radians);

        sumCos += (float) Math.cos(radians);
        if(queue.size()==0){
        	first = radians;
        }
        queue.add(radians);
        
        if(queue.size() > LENGTH){

            float old = queue.poll();

            sumSin -= Math.sin(old)*smooth;

            sumCos -= Math.cos(old)*smooth;
        }
    }

    public float average(){

        int size = queue.size();
        return (float) (Math.atan2(sumSin / size, sumCos / size));
    }
}
