package com.example.sightcity;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ClipData.Item;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.sightcity.services.CommonConstants;
import com.example.sightcity.services.StreetPassController;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends Activity implements ActionBar.TabListener {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a {@link FragmentPagerAdapter}
	 * derivative, which will keep every loaded fragment in memory. If this
	 * becomes too memory intensive, it may be best to switch to a
	 * {@link android.support.v13.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;
	// listeners
	CustomPlaceListener placelistener;
	CustomSensorEventListener sensorlistenr;
	CustomCompassListener compasslistener;
	ViewPager mViewPager;
	// managers
	LocationManager locationManager;
	SensorManager sensorManager;
	// sensors
	Sensor Accelerometer;
	Sensor Compass;
	// places
	public PlacesController placesC;
	static Intent placePicker;
	// values
	public static float lastAccelerometer[];
	public static float lastCompass[];
	public static Location lastCoordinate;

	float LastOrientation[];

	AngleLowpassFilter[] lowpass;
	public static boolean canStartDrawingPlaces;
	public static boolean gotLocation;
	public static boolean gotAcc;
	public static MainActivity instance;
	private StreetPassController controller;

	ActionBar actionBar;
	public static boolean isVisible = false;
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Set up the action bar.
		actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#ff4ba587")));
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the activity.
		mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}
		// setting up gps
		locationManager = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		criteria.setPowerRequirement(Criteria.NO_REQUIREMENT);
		placelistener = new CustomPlaceListener(this);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
				0, placelistener);
		locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 0, 0, placelistener);
		// setting up accelerometer
		sensorlistenr = new CustomSensorEventListener(this);
		sensorManager = (SensorManager) this
				.getSystemService(Context.SENSOR_SERVICE);
		Accelerometer = sensorManager
				.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		sensorManager.registerListener(sensorlistenr, Accelerometer,
				SensorManager.SENSOR_DELAY_NORMAL);
		// setting up comapss
		compasslistener = new CustomCompassListener(this);
		Compass = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		sensorManager.registerListener(compasslistener, Compass,
				SensorManager.SENSOR_DELAY_NORMAL);
		// setting up places
		placesC = new PlacesController(this);
		int PLACE_PICKER_REQUEST = 1;
		PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
		instance = this;
		try {
			placePicker = builder.build(this);
			// startActivityForResult(placePicker, PLACE_PICKER_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
		}
		toggleGPS(true);
		controller = new StreetPassController(this);
		controller.startStreetPass();

	}

	@Override
	public void onResume() {
		isVisible = true;
		try{
		boolean open = getIntent().getBooleanExtra("open_tab", false);
		if (open) {
			actionBar.setSelectedNavigationItem(0);
			//getIntent().putExtra("open_tab",false);
		}
		}catch(Exception e){
			
		}
		try{
		if(locationManager!=null){
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
				0, placelistener);
		locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 0, 0, placelistener);
		}
		}
		catch(Exception e){
			
		}
		super.onResume();
	}

	@Override
	public void onPause() {
		isVisible = false;
		try{
		if(locationManager!=null){
		locationManager.removeUpdates(placelistener);
		}
		}
		catch(Exception e){
			
		}
		super.onPause();
	}

	@Override
	protected void onStart() {
		super.onStart();
		placesC.start();
	}

	@Override
	protected void onStop() {
		placesC.stop();
		super.onStop();
	}

	private void toggleGPS(boolean enable) {
		LocationManagerCheck locationManagerCheck = new LocationManagerCheck(
				this);
		Location location = null;
		if (locationManagerCheck.isLocationServiceAvailable()) {

			if (locationManagerCheck.getProviderType() == 1)
				location = locationManager
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			else if (locationManagerCheck.getProviderType() == 2)
				location = locationManager
						.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		} else {
			locationManagerCheck.createLocationServiceError(this);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent activityIntent;
			activityIntent = new Intent(this, SettingsActivity.class);
			activityIntent = activityIntent
					.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(activityIntent);
			overridePendingTransition(R.anim.slide_in,
					R.anim.slide_out);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	/*
	 * public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
	 * int width = bm.getWidth(); int height = bm.getHeight(); float scaleWidth
	 * = ((float) newWidth) / width; float scaleHeight = ((float) newHeight) /
	 * height; // create a matrix for the manipulation Matrix matrix = new
	 * Matrix(); // resize the bit map matrix.postScale(scaleWidth,
	 * scaleHeight); // recreate the new Bitmap Bitmap resizedBitmap =
	 * Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false); return
	 * resizedBitmap; }
	 */

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a PlaceholderFragment (defined as a static inner class
			// below).

			return PlaceholderFragment.newInstance(position + 1);
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			}
			return null;
		}
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment implements
			SurfaceHolder.Callback {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";
		private SurfaceView surfaceView;
		private SurfaceHolder surfaceHolder;

		Camera camera;
		public static float verticalFOV;
		public static float horizontalFOV;
		Camera.PictureCallback rawCallback;
		Camera.ShutterCallback shutterCallback;
		Camera.PictureCallback jpegCallback;
		public static GoogleMap mMap;
		public static boolean animated = false;
		public int topconv;
		public int oldY=0;
		public static View rootView = null;
		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static PlaceholderFragment newInstance(int sectionNumber) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			animated = false;
			return fragment;
		}

		public PlaceholderFragment() {

		}

		public static void setUpMapIfNeeded() {
			if (mMap == null) {
				mMap = ((MapFragment) MainActivity.instance
						.getFragmentManager().findFragmentById(R.id.map))
						.getMap();
			}
			setUpMap();
		}

		public static void setUpMap() {
			mMap.setMyLocationEnabled(true);
			if (gotLocation) {
				LatLng latlng = new LatLng(lastCoordinate.getLatitude(),
						lastCoordinate.getLongitude());
				if (!animated) {
					mMap.addMarker(new MarkerOptions().position(latlng)
							.title("My Location").snippet("Home Address"));
					mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
							latlng, 13.0f));
					animated = true;
				}
			}
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			int section = this.getArguments().getInt(ARG_SECTION_NUMBER);
			rootView = null;
			switch (section) {
			case 1:
				rootView = inflater.inflate(R.layout.fragment_main, container,
						false);
				break;
			default:
				try {
					rootView = inflater.inflate(R.layout.fragment_camera,
							container, false);
					surfaceView = (SurfaceView) rootView
							.findViewById(R.id.surfaceView);
					surfaceHolder = surfaceView.getHolder();
					surfaceHolder.addCallback(this);
					LinearLayout tapview = (LinearLayout) rootView
							.findViewById(R.id.informationHolder);
					Display display = MainActivity.instance.getWindowManager().getDefaultDisplay();
					Point size = new Point();
					display.getSize(size);
					topconv = size.y;
					topconv-=330;
					if(tapview.getLayoutParams() instanceof ViewGroup.MarginLayoutParams){
						ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tapview.getLayoutParams();
				        p.setMargins(0, topconv, 0, 0);
				        tapview.requestLayout();
					}
					//image.dra
					tapview.setOnTouchListener(new OnTouchListener() {
						public float oldX;
						@Override
						public boolean onTouch(View v, MotionEvent event) {
							
							Display display = MainActivity.instance.getWindowManager().getDefaultDisplay();
							Point size = new Point();
							display.getSize(size);
							// TODO Auto-generated method stub

							ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
							switch(event.getAction()){
							case MotionEvent.ACTION_DOWN:
								break;
							case MotionEvent.ACTION_MOVE:
								oldX = event.getRawY()-144;
								if(oldX>0){
								 p.setMargins(0,  (int)oldX, 0, 0);
						         v.requestLayout();
								}
								//event.getY();
								break;
							case MotionEvent.ACTION_UP:
								if(oldX!=0 && oldX<400){
									p.setMargins(0,  0, 0, 0);
							         v.requestLayout();
								}
								else{
									p.setMargins(0,  topconv, 0, 0);
							         v.requestLayout();
								}
								break;
							}
							
							return true;
						}
					});
					tapview.setOnDragListener(new OnDragListener() {
						
						@Override
						public boolean onDrag(View v, DragEvent event) {
							// TODO Auto-generated method stub
							switch(event.getAction()){
							case DragEvent.ACTION_DRAG_STARTED:
								ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
						        p.setMargins(0, topconv, 0, 0);
						        v.requestLayout();
								 p.setMargins(0, topconv++, 0, 0);
								break;
							case DragEvent.ACTION_DRAG_ENDED:
								break;
							}
							return false;
						}
					});
					// surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
					jpegCallback = new Camera.PictureCallback() {
						@Override
						public void onPictureTaken(byte[] data, Camera camera) {
							FileOutputStream outStream = null;
							try {
								outStream = new FileOutputStream(String.format(
										"/sdcard/%d.jpg",
										System.currentTimeMillis()));

								outStream.write(data);
								outStream.close();
							}

							catch (FileNotFoundException e) {
								e.printStackTrace();
							}

							catch (IOException e) {
								e.printStackTrace();
							}

							finally {
							}
							refreshCamera();
						}
					};
				} catch (Exception e) {
					String message = e.getMessage();
					Log.println(1, "error", message);
				}
				break;
			}
			return rootView;
		}

		public void captureImage(View v) throws IOException {
			camera.takePicture(null, null, jpegCallback);
		}

		public void refreshCamera() {
			if (surfaceHolder.getSurface() == null) {
				return;
			}

			try {
				camera.stopPreview();
			}

			catch (Exception e) {
			}

			try {
				camera.setPreviewDisplay(surfaceHolder);
				camera.startPreview();
			} catch (Exception e) {
			}
			setCameraDisplayOrientation(this.getActivity(), 1, camera);
		}

		public static void setCameraDisplayOrientation(Activity activity,
				int cameraId, android.hardware.Camera camera) {
			android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
			android.hardware.Camera.getCameraInfo(cameraId, info);
			int rotation = activity.getWindowManager().getDefaultDisplay()
					.getRotation();
			int degrees = 0;
			switch (rotation) {
			case Surface.ROTATION_0:
				degrees = 0;
				break;
			case Surface.ROTATION_90:
				degrees = 90;
				break;
			case Surface.ROTATION_180:
				degrees = 180;
				break;
			case Surface.ROTATION_270:
				degrees = 270;
				break;
			}

			int result;
			if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
				result = (info.orientation + degrees) % 360;
				result = (360 - result) % 360; // compensate the mirror
			} else { // back-facing
				result = (info.orientation - degrees + 360) % 360;
			}
			camera.setDisplayOrientation(result);
		}

		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			try {
				camera = Camera.open();
			}

			catch (RuntimeException e) {
				System.err.println(e);
				return;
			}

			Camera.Parameters param;
			param = camera.getParameters();
			param.setPreviewSize(352, 288);
			camera.setParameters(param);

			verticalFOV = param.getVerticalViewAngle();
			horizontalFOV = param.getHorizontalViewAngle();
			MainActivity.canStartDrawingPlaces = true;

			try {
				camera.setPreviewDisplay(surfaceHolder);
				camera.startPreview();
			}

			catch (Exception e) {
				System.err.println(e);
				return;
			}
		}

		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width,
				int height) {
			refreshCamera();
		}

		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			camera.stopPreview();
			camera.release();
			camera = null;
		}
	}
	@Override
    public void onBackPressed() {        // to prevent irritating accidental logouts
        SearchView.instance.changeGif(CommonConstants.GIF_Normal);
            super.onBackPressed();       // bye

    }

}
