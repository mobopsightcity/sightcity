package com.example.sightcity;

import com.example.sightcity.MainActivity.PlaceholderFragment;

import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class CustomPlaceListener implements LocationListener {
	Activity activity;
	public static double oldAltitude = 0;
	public CustomPlaceListener(Activity act) {
		this.activity = act;
		/*MainActivity.lastCoordinate = MainActivity.instance.locationManager
				.getLastKnownLocation(LocationManager.GPS_PROVIDER);*/
	}

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
		try {
			MainActivity.lastCoordinate = arg0;
			MainActivity.lastCoordinate.setAltitude(oldAltitude);
			MainActivity.gotLocation = true;
			PlaceholderFragment.setUpMapIfNeeded();
			MainActivity.instance.placesC.getPlaces(this.activity);
			PlacesController.instance.getAltitude();
			/*
			*/
		} catch (Exception e) {

		}
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub

	}
}