package com.example.sightcity;

import java.util.ArrayList;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.example.sightcity.services.CommonConstants;

public class SearchActivity extends Activity implements OnItemClickListener{
	private SpeechRecognizer speachRecognizer;
	private SpeechRecognizerlistener listener;
	public static boolean mIsListening = false;
	public static boolean canChangeGif = false;
	public static SearchActivity instance;
	private ListView listView;
	public Location oldLocation;
	public boolean gotLocation=false;
	public static boolean isVisible;
	public boolean keyboardVisible;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
		initializeSpeach();
		instance = this;
		WelcomePageActivity.instance.finish();
		ImageButton speak_now = (ImageButton) this.findViewById(R.id.btnSpeak);
		speak_now.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					startListening();
					break;
				case MotionEvent.ACTION_UP:
					//stop();
					break;
				}
				return false;
			}
		});
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		listView = (ListView) findViewById(R.id.commands);
        listView.setOnItemClickListener(this);
        initiliazeList();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        
        
        CustomEditText input = (CustomEditText)findViewById(R.id.search_for);
   
        input.setOnEditorActionListener(new OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				Intent activityIntent;
				if (MainActivity.instance == null) {
					activityIntent = new Intent(SearchActivity.instance, MainActivity.class);
				} else {
					activityIntent = MainActivity.instance.getIntent();
				}
				// TODO Auto-generated method stub
				switch(actionId){
				case EditorInfo.IME_ACTION_DONE: 
					if(keyboardVisible){
						keyboardVisible= false;
						RelativeLayout layout = (RelativeLayout)findViewById(R.id.search_for_holder);
						if(layout.getLayoutParams() instanceof ViewGroup.MarginLayoutParams){
							ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) layout.getLayoutParams();
					        p.setMargins(0, 0, 0, 0);
					        layout.requestLayout();
						}
					}
					CustomEditText input = (CustomEditText)findViewById(R.id.search_for);
					activityIntent.putExtra("open_tab", true);
					activityIntent.putExtra("search_for_type", "");
					String inputText = input.getText().toString();
					activityIntent.putExtra("search_for_specific", inputText);
					activityIntent = activityIntent
							.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
					SearchView.instance.changeGif(CommonConstants.GIF_SEARCHING);
					SearchActivity.mIsListening = false;
					SearchActivity.instance.startActivity(activityIntent);
					SearchActivity.instance.overridePendingTransition(R.anim.slide_in,
							R.anim.slide_out);
				break;
				}

				return false;
			}
		});
        input.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!keyboardVisible){
					keyboardVisible=true;
					RelativeLayout layout = (RelativeLayout)findViewById(R.id.search_for_holder);
					if(layout.getLayoutParams() instanceof ViewGroup.MarginLayoutParams){
						ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) layout.getLayoutParams();
				        p.setMargins(0, 0, 0, 338);
				        layout.requestLayout();
					}
					
				}
				else{
				Toast.makeText(SearchActivity.instance, "Keyboard already shown",Toast.LENGTH_SHORT).show();
				}
			}
		});
 
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}
	@Override
	protected void onPause() {
		super.onPause();
		isVisible = false;
	};
	@Override
	protected void onResume() {
		super.onResume();
		isVisible=true;
		RelativeLayout layout = (RelativeLayout)findViewById(R.id.search_for_holder);
		if(layout.getLayoutParams() instanceof ViewGroup.MarginLayoutParams){
			ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) layout.getLayoutParams();
	        p.setMargins(0, 0, 0, 0);
	        layout.requestLayout();
		}
		try{
		View view = SearchActivity.instance.getCurrentFocus();
		InputMethodManager imm = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
		catch(Exception e){
			
		}
	};
	
	private void initiliazeList(){
		ArrayList<ListItem> arrayOfItems = new ArrayList<ListItem>();
		String[] array = getResources().getStringArray(R.array.place_type_name);  
		for (String string : array) {
			arrayOfItems.add(new ListItem(string));
		}
		
		ListItemAdapter adapter = new ListItemAdapter(this, arrayOfItems);
		// Attach the adapter to a ListView
		ListView listView = (ListView) findViewById(R.id.commands);
		listView.setAdapter(adapter);
	}
	private void initializeSpeach() {
		speachRecognizer = SpeechRecognizer.createSpeechRecognizer(this);
		listener = new SpeechRecognizerlistener(this);
		speachRecognizer.setRecognitionListener(listener);
	}

	public void startListening() {
		if(!mIsListening){
			try{
				keyboardVisible = false;
				View view = getCurrentFocus();
				InputMethodManager imm = (InputMethodManager)
		                SearchActivity.instance.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
				}
				catch(Exception e){
					
				}
			RelativeLayout layout = (RelativeLayout)findViewById(R.id.search_for_holder);
			if(layout.getLayoutParams() instanceof ViewGroup.MarginLayoutParams){
				ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) layout.getLayoutParams();
		        p.setMargins(0, 0, 0, 0);
		        layout.requestLayout();
			}
		SearchView.instance.changeGif(CommonConstants.GIF_GETTING_READY);
		//listener.playAudio(R.raw.ok_hit_me);
		mIsListening = true;
		initializeSpeach();
		Intent recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,Locale.getDefault());
		recognizerIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_MINIMUM_LENGTH_MILLIS,5000);
			recognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,"com.example.sightcity");
		speachRecognizer.startListening(recognizerIntent);
		}
	}

	public void stop() {
		if (speachRecognizer != null) {
			speachRecognizer.stopListening();
			speachRecognizer.cancel();
			speachRecognizer.destroy();

			//mIsListening = false;
			/*
			 * if (Build.VERSION.SDK_INT >=
			 * 16);//Build.VERSION_CODES.JELLY_BEAN)
			 * mAudioManager.setStreamMute(AudioManager.STREAM_SYSTEM, false);
			 */
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}
}
