package com.example.sightcity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.sightcity.MainActivity.PlaceholderFragment;
import com.example.sightcity.places.PlaceDetailsJSONParser;
import com.example.sightcity.places.PlaceJSONParser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class PlacesController implements ConnectionCallbacks,
		OnConnectionFailedListener {
	private GoogleApiClient mGoogleApiClient;
	private boolean gotPlaces = false;
	private boolean gotAltitude = false;
	private Context context;
	public List<HashMap<String, String>> places;
	public static PlacesController instance;
	
	public PlacesController(Context context) {
		this.context = context;
		mGoogleApiClient = new GoogleApiClient.Builder(context)
				.addApi(Places.GEO_DATA_API).addApi(Places.PLACE_DETECTION_API)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this).build();
		instance = this;
	}

	public void start() {
		mGoogleApiClient.connect();
	}

	public void stop() {
		mGoogleApiClient.disconnect();
	}

	public void getAltitude() {
		if (gotPlaces && !gotAltitude) {
			StringBuilder sb = new StringBuilder(
					"https://maps.googleapis.com/maps/api/elevation/json?");
			sb.append("locations=");
			for (int i = 0; i < places.size(); i++) {
				if (i == 0) {
					sb.append(places.get(i).get("lat") + ","
							+ places.get(i).get("lng"));
				} else {
					sb.append("|" + places.get(i).get("lat") + ","
							+ places.get(i).get("lng"));
				}
			}
			sb.append("|" + MainActivity.lastCoordinate.getLatitude() + ","
					+ MainActivity.lastCoordinate.getLongitude());
			AltitudeTask altitudeTask = new AltitudeTask();
			altitudeTask.execute(sb.toString());
			gotAltitude = true;
		}
	}
	public void getPlaceImage(String placeId){
		StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/photo?");
		sb.append("maxwidth=540");
		sb.append("&photoreference="+placeId);
		sb.append("&key=AIzaSyAz8k2uOYSD3Y32kXoLboaQtOrNEZXcS3c");
		ImageTask task  = new ImageTask();
		task.execute(sb.toString());
	}
	public void getPlaces(Activity context) {
		if (!gotPlaces) {
			gotAltitude = false;
			StringBuilder sb = new StringBuilder(
					"https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
			sb.append("location=" + MainActivity.lastCoordinate.getLatitude()
					+ "," + MainActivity.lastCoordinate.getLongitude());
			SharedPreferences preferences = MainActivity.instance.getSharedPreferences("SettingsActivity", MainActivity.instance.MODE_PRIVATE);
			int radius = preferences.getInt("Radius", 500);
			sb.append("&radius="+radius);
			String type= context.getIntent().getStringExtra("search_for_type");
			String specific = context.getIntent().getStringExtra("search_for_specific");
			if(specific!=null && !specific.equals("")){
				sb.append("&name="+specific.toLowerCase());
			}
			if(type!=null && !type.equals("")){
				sb.append("&types="+type.toLowerCase());
			}
			sb.append("&key=AIzaSyAz8k2uOYSD3Y32kXoLboaQtOrNEZXcS3c");
			PlacesTask placesTask = new PlacesTask();
			// Invokes the "doInBackground()" method of the class PlaceTask
			placesTask.execute(sb.toString());
			gotPlaces = true;
		}
	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub

	}

	/** A method to download json data from url */
	private String downloadUrl(String strUrl) throws IOException {
		String data = "";
		InputStream iStream = null;
		HttpURLConnection urlConnection = null;
		try {
			URL url = new URL(strUrl);

			// Creating an http connection to communicate with url
			urlConnection = (HttpURLConnection) url.openConnection();

			// Connecting to url
			urlConnection.connect();

			// Reading data from url
			iStream = urlConnection.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(
					iStream));

			StringBuffer sb = new StringBuffer();

			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

			data = sb.toString();
			br.close();

		} catch (Exception e) {
			Log.d("Exception while downloading url", e.toString());
		} finally {
			iStream.close();
			urlConnection.disconnect();
		}
		return data;
	}
	private class ImageTask extends AsyncTask<String, Integer, Bitmap>{
		@Override
		protected Bitmap doInBackground(String... params) {
			// TODO Auto-generated method stub
			try{
			URL url = new URL(params[0]);

			// Creating an http connection to communicate with url
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			// Connecting to url
			urlConnection.connect();

			// Reading data from url
			InputStream iStream = urlConnection.getInputStream();
			return BitmapFactory.decodeStream(iStream);
			}
			catch(Exception e){
				return BitmapFactory.decodeResource(MainActivity.instance.getResources(), R.drawable.noimg);
			}
		}
		private int dpToPx(int dp) {
		    float density = MainActivity.instance.getResources().getDisplayMetrics().density;
		    return Math.round((float)dp * density);
		}
		@Override
		protected void onPostExecute(Bitmap result) {
			try{
				int width = 540;
				int height = 330;
			    int boundingx = dpToPx(width);
			    int boundingy = dpToPx(height);

			Bitmap scaledBitmap = Bitmap.createScaledBitmap(result, boundingx, boundingy, false);
			ImageView imgview = (ImageView)PlaceholderFragment.rootView.findViewById(R.id.informationImage);
			imgview.setImageBitmap(scaledBitmap);
			}
			catch(Exception e){
				
			}
		}
		
	}
	private class AltitudeTask extends AsyncTask<String, Integer, String> {
		String data = null;

		@Override
		protected String doInBackground(String... url) {
			// TODO Auto-generated method stub
			try {
				data = downloadUrl(url[0]);
			} catch (Exception e) {
				Log.d("Background Task", e.toString());
			}
			return data;
		}

		// Executed after the complete execution of doInBackground() method
		@Override
		protected void onPostExecute(String result) {
			try {
				JSONObject jObject = new JSONObject(result);
				JSONArray results = jObject.getJSONArray("results");
				for (int i = 0; i < results.length(); i++) {
					if (i != results.length() - 1)
						places.get(i)
								.put("alt",
										results.getJSONObject(i).getString(
												"elevation"));
					else {
						CustomPlaceListener.oldAltitude = Double
								.parseDouble(results.getJSONObject(i)
										.getString("elevation"));
					}
				}
				SightsController.instance.fetchSights();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				SightsController.instance.fetchSights();
				e.printStackTrace();
			}
		}

	}

	private class PlacesTask extends AsyncTask<String, Integer, String> {

		String data = null;

		// Invoked by execute() method of this object
		@Override
		protected String doInBackground(String... url) {
			try {
				data = downloadUrl(url[0]);
			} catch (Exception e) {
				Log.d("Background Task", e.toString());
			}
			return data;
		}

		// Executed after the complete execution of doInBackground() method
		@Override
		protected void onPostExecute(String result) {
			ParserTask parserTask = new ParserTask();

			// Start parsing the Google place details in JSON format
			// Invokes the "doInBackground()" method of the class ParseTask
			parserTask.execute(result);
		}
	}

	private class ParserTask extends
			AsyncTask<String, Integer, List<HashMap<String, String>>> {

		JSONObject jObject;

		// Invoked by execute() method of this object
		@Override
		protected List<HashMap<String, String>> doInBackground(
				String... jsonData) {

			PlaceJSONParser placeJsonParser = new PlaceJSONParser();

			try {
				jObject = new JSONObject(jsonData[0]);

				/** Getting the parsed data as a List construct */
				places = placeJsonParser.parse(jObject);

			} catch (Exception e) {
				places = new ArrayList<HashMap<String, String>>();
				Log.d("Exception", e.toString());
			}
			return places;
		}

		// Executed after the complete execution of doInBackground() method
		@Override
		protected void onPostExecute(List<HashMap<String, String>> list) {
			if (MainActivity.isVisible) {
				// Clears all the existing markers
				PlaceholderFragment.mMap.clear();

				for (int i = 0; i < list.size(); i++) {

					// Creating a marker
					MarkerOptions markerOptions = new MarkerOptions();

					// Getting a place from the places list
					HashMap<String, String> hmPlace = list.get(i);

					// Getting latitude of the place
					double lat = Double.parseDouble(hmPlace.get("lat"));

					// Getting longitude of the place
					double lng = Double.parseDouble(hmPlace.get("lng"));

					// Getting name
					String name = hmPlace.get("place_name");

					// Getting vicinity
					String vicinity = hmPlace.get("vicinity");

					LatLng latLng = new LatLng(lat, lng);

					// Setting the position for the marker
					markerOptions.position(latLng);

					// Setting the title for the marker.
					// This will be displayed on taping the marker
					markerOptions.title(name + " : " + vicinity);

					// Placing a marker on the touched position
					Marker m = PlaceholderFragment.mMap
							.addMarker(markerOptions);

					// Linking Marker id and place reference
					// mMarkerPlaceLink.put(m.getId(),
					// hmPlace.get("reference"));
				}
			} else {
				Toast.makeText(context, "runing in background", Toast.LENGTH_SHORT).show();
			}
		}
	}
}
