package com.example.sightcity;

import java.io.InputStream;

import com.example.sightcity.services.CommonConstants;
import com.example.sightcity.services.StreetPassService;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;

public class WelcomePageActivity extends Activity {
	public static WelcomePageActivity instance;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.welcomepage);
		ImageView view = (ImageView)this.findViewById(R.id.imageWelcome);
		InputStream is = this.getResources().openRawResource(R.drawable.leaf);
		view.setBackground(Drawable.createFromStream(is, "leaf"));
		AlarmManager alarmManager = (AlarmManager) this
				.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(this, SearchActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,0);
		alarmManager.setExact(AlarmManager.RTC_WAKEUP,
				System.currentTimeMillis()+4000, pendingIntent);
		this.overridePendingTransition(R.anim.slide_in,
				R.anim.slide_out);
		instance = this;
		
	}
	
}
