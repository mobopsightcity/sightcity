package com.example.sightcity.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class StreetPassBroadcastReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context arg0, Intent arg1) {
		// TODO Auto-generated method stub
		// PlacesController.instance.getPlaces();
		AlarmManager alarmManager = (AlarmManager) arg0
				.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(arg0, StreetPassService.class);
		PendingIntent pendingIntent = PendingIntent.getService(arg0, 0, intent,
				0);
		alarmManager.setExact(AlarmManager.RTC_WAKEUP,
				System.currentTimeMillis()+CommonConstants.UPDATE_INTERVAL, pendingIntent);
	}

}
