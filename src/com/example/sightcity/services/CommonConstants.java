package com.example.sightcity.services;

import com.example.sightcity.R;

public final class CommonConstants {

    public CommonConstants() {

        // don't allow the class to be instantiated
    }

    // Milliseconds in the snooze duration, which translates
    // to 20 seconds.

	public static int UPDATE_INTERVAL = 120000;
    public static final int SNOOZE_DURATION = 20000;
    public static final int DEFAULT_TIMER_DURATION = 10000;
    public static final String ACTION_SNOOZE = "com.example.android.sightcity.ACTION_SNOOZE";
    public static final String ACTION_DISMISS = "com.example.android.sightcity.ACTION_DISMISS";
    public static final String ACTION_CONTINUE = "com.example.android.sightcity.ACTION_CONTINUE";
    public static final String EXTRA_MESSAGE= "com.example.android.sightcity.EXTRA_MESSAGE";
    public static final String EXTRA_TIMER = "com.example.android.sightcity.EXTRA_TIMER";
    public static final int NOTIFICATION_ID = 001;
    public static final String DEBUG_TAG = "PingMe";
    public static final int GIF_SEARCHING = R.drawable.jarvis_searching;
    public static final int GIF_Normal = R.drawable.jarvis_normal;
    public static final int GIF_LISTENING = R.drawable.jarvis_listening;
	public static final int GIF_GETTING_READY = R.drawable.jarvis_getting_ready;
}