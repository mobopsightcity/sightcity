package com.example.sightcity.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class StreetPassController {
	//private PendingIntent mServiceIntent;
	public static final String PREFS_NAME = "SighSettings";
	public static Context context;
	private boolean registered = false;
	public StreetPassController(Context context){
		this.context = context;
	}
	public void startStreetPass() {
		SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(context);
		boolean firstRun = p.getBoolean("PREFERENCE_FIRST_RUN", true);
		if(firstRun){
			AlarmManager alarmManager = (AlarmManager) context
					.getSystemService(Context.ALARM_SERVICE);
			Intent intent = new Intent(context, StreetPassService.class);
			PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent,
					0);
			alarmManager.setExact(AlarmManager.RTC_WAKEUP,
					System.currentTimeMillis()+ CommonConstants.UPDATE_INTERVAL, pendingIntent);
			p.edit().putBoolean("PREFERENCE_FIRST_RUN", false).commit();
		}
	}
	public void cancelStreetPass() {
		
	}
}
