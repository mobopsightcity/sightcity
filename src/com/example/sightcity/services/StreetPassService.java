package com.example.sightcity.services;

import com.example.sightcity.MainActivity;
import com.example.sightcity.R;
import com.example.sightcity.SearchActivity;
import com.example.sightcity.SettingsActivity;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

public class StreetPassService extends IntentService {
	public static int counter = 0;

	public StreetPassService() {
		super("StreetPassService");
		counter++;
	}

	private void createNotification() {
		try{
		Intent activityIntent;
		if(MainActivity.instance==null){
			activityIntent = new Intent(getApplicationContext(),MainActivity.class);
		}
		else{
			activityIntent = MainActivity.instance.getIntent();
		}
		activityIntent.putExtra("open_tab", true);
		activityIntent = activityIntent
				.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent wakeup = PendingIntent.getActivity(this, 0,
				activityIntent, 0);
		Notification noti = new Notification.Builder(getApplicationContext())
				.setContentTitle("Dont miss out!")
				.setContentText("Some exciting places to check nearby!")
				.setSmallIcon(R.drawable.leaf_logo).setShowWhen(true).setWhen(0)
				.setAutoCancel(true)
				.setDefaults(Notification.DEFAULT_VIBRATE|Notification.DEFAULT_SOUND)
				.setPriority(Notification.PRIORITY_MAX)
				.setContentIntent(wakeup)
				.build();
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE);
		// mId allows you to update the notification later on.
		mNotificationManager.notify(CommonConstants.NOTIFICATION_ID, noti);
		}
		catch(Exception e)
		{
			Toast.makeText(getApplicationContext(),
					"error creating notification...\n"+e.getMessage(),
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void onHandleIntent(Intent workIntent) {
		SharedPreferences preferences = getSharedPreferences("SettingsActivity", MODE_PRIVATE);
		boolean sendNotification = preferences.getBoolean("sendNotifications", false);
		if (!MainActivity.isVisible && !SearchActivity.isVisible &&!SettingsActivity.isVisible && sendNotification) {
			
			createNotification();
		} 
		//save All fetched sights
		Context context = getApplicationContext();
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, StreetPassService.class);
		PendingIntent pendingIntent = PendingIntent.getService(context, 0,
				intent, 0);
		alarmManager.setExact(AlarmManager.RTC_WAKEUP,
				System.currentTimeMillis() + CommonConstants.UPDATE_INTERVAL, pendingIntent);
	}

	@Override
	public void onDestroy() {
		counter--;
	}
}
