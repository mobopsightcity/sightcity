package com.example.sightcity;

import android.location.Location;

public class Sight {
	public enum sightType{
		nature, urban, caffe
	}
	public Location coord;
	public float LastOrientation[];
	public AngleLowpassFilter[] lowpass;
	public float curBearingToThisSight;
	public sightType type;
	public String name;
	public String vicinity;
	public String iconUrl;
	public String imageReference;
	public int bitmapId;
	public float xOnScreen;
	public float yOnScreen;
}
