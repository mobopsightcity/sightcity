package com.example.sightcity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;

public class SettingsActivity extends Activity {
	public static SettingsActivity instance;
	private ActionBar actionBar;
	public static boolean isVisible;
	public int step = 500;
	public int max = 2500;
	public int min = 500;
	int radius;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		instance = this;
		setContentView(R.layout.activity_settings);
		actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color
				.parseColor("#ff4ba587")));
		SharedPreferences preferences = getPreferences(MODE_PRIVATE);
		Switch inputSwitch = (Switch)findViewById(R.id.switchNotification);
		Switch suggestionSwitch = (Switch)findViewById(R.id.switchSuggestion);
		SeekBar bar = (SeekBar)findViewById(R.id.seekBar1);

		bar.setMax((max-min)/500);
		bar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				 radius = min + (progress * step);
				 TextView textradius = (TextView)findViewById(R.id.distanceText);
				 textradius.setText("Distance Radius: ("+radius+" M)");
			}
		});
		if(preferences.contains("sendNotifications")){
			boolean sendNotifications = preferences.getBoolean("sendNotifications", false);
			boolean showSuggestions = preferences.getBoolean("showSuggestions", false);
			TextView textNoti = (TextView)findViewById(R.id.textNotifications);
			TextView textSugg = (TextView)findViewById(R.id.textSuggestion);
			textNoti.setText(sendNotifications==true? R.string.off_notifications:R.string.on_notifications);
			textSugg.setText(showSuggestions==true? R.string.off_suggestions:R.string.on_suggestions);
			inputSwitch.setChecked(sendNotifications);
			suggestionSwitch.setChecked(showSuggestions);
			radius = preferences.getInt("Radius", 500);
			bar.setProgress((radius-min)/step);
		}
		else{
			Editor edit = preferences.edit();
			edit.putBoolean("sendNotifications", false);
			edit.putBoolean("showSuggestions", false);
			edit.putInt("Radius", 500);
			edit.apply();
		}
		Button saveButton = (Button)findViewById(R.id.saveSetting);
		inputSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				TextView textNoti = (TextView)findViewById(R.id.textNotifications);
				textNoti.setText(isChecked? R.string.off_notifications:R.string.on_notifications);
				
			}
		});
		suggestionSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				TextView textSugg = (TextView)findViewById(R.id.textSuggestion);
				textSugg.setText(isChecked? R.string.off_suggestions:R.string.on_suggestions);
			}
		});
		saveButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SharedPreferences preferences = getPreferences(MODE_PRIVATE);
				
				Switch notificationSwitch = (Switch)findViewById(R.id.switchNotification);
				Switch suggestionSwitch = (Switch)findViewById(R.id.switchSuggestion);
				Editor edit = preferences.edit();
				edit.putBoolean("sendNotifications", notificationSwitch.isChecked());
				edit.putBoolean("showSuggestions", suggestionSwitch.isChecked());
				edit.putInt("Radius", radius);
				edit.apply();
				LayoutInflater inflater = getLayoutInflater();
				View layout = inflater.inflate(R.layout.toastlayout,
				                               (ViewGroup) findViewById(R.id.toast_layout_root));
				TextView text = (TextView) layout.findViewById(R.id.text);
				text.setText("Settings Saved");
				Toast toast = new Toast(getApplicationContext());
				toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
				toast.setDuration(Toast.LENGTH_LONG);
				toast.setView(layout);
				toast.show();
				onBackPressed();
			}
		});

	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		isVisible=false;
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		isVisible = true;
	}
}
